# linear regression

import numpy as np


class linear:

    def fit(self, x, y, n=500, a=.5):
        self.w = np.random.randn(x.shape[1])

        for i in range(n):

            loss = self.forward(x) - y
            dw = loss * (.1 / len(x))

            self.w -= a * dw @ x

    def forward(self, x):
        return x @ self.w