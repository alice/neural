# perceptron algorithm

import numpy as np


class perceptron:

    def fit(self, x, y, n=500, a=.2):
        self.w = np.random.randn(x.shape[1])

        for i in range(n):
            self.w += a * (y - self.forward(x)) @ x

    def forward(self, x):
        return (x @ self.w) > 0
