# a multilayer perceptron
# activation function: sigmoid
# uses biases: no


from fun import *


class ann:

  def __init__(self, *shape):
    self.w = list(map(randn, shape[:-1], shape[1:]))


  def forward(self, v):

    for w in self.w:
      v.append(sigma(v[-1] @ w))

    return v


  def back(self, y, v):
    error = v.pop() - y

    for w, o in revzip(self.w, v):
      pd = o.T @ error

      error = dsigma(o) * (error @ w.T)
      w -= .05 * pd


  def fit(self, x, y, n):

    for i in range(n):
      self.back(y, self.forward([x]))


  def predict(self, x):
    return self.forward([x]).pop()